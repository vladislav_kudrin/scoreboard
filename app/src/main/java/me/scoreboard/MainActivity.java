package me.scoreboard;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import java.util.Locale;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03/11/2021
 */
public class MainActivity extends AppCompatActivity {
    private int usaScore = 0;
    private int canadaScore = 0;
    private TextView usaView, canadaView;
    private Button resetButton;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usaView = findViewById(R.id.usaScore);
        canadaView = findViewById(R.id.canadaScore);
        resetButton = findViewById(R.id.resetButton);
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        usaScore = savedInstanceState.getInt("USA_SCORE_STATE_KEY");
        usaView.setText(String.format(Locale.getDefault(), "%d", usaScore));
        canadaScore = savedInstanceState.getInt("CANADA_SCORE_STATE_KEY");
        canadaView.setText(String.format(Locale.getDefault(), "%d", canadaScore));
        resetButton.setVisibility(savedInstanceState.getInt("RESET_BUTTON_VISIBILITY_STATE_KEY"));
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("USA_SCORE_STATE_KEY", usaScore);
        outState.putInt("CANADA_SCORE_STATE_KEY", canadaScore);
        outState.putInt("RESET_BUTTON_VISIBILITY_STATE_KEY", resetButton.getVisibility());
        super.onSaveInstanceState(outState);
    }

    /**
     * Increases score of the chosen team and makes {@code resetButton} visible if it wasn't.
     *
     * @param view user interface components.
     */
    public void onClickTeamButton(View view) {
        if (view.getId() == R.id.usaButton) {
            usaScore++;
            usaView.setText(String.format(Locale.getDefault(), "%d", usaScore));
        } else {
            canadaScore++;
            canadaView.setText(String.format(Locale.getDefault(), "%d", canadaScore));
        }

        if (resetButton.getVisibility() != View.VISIBLE) resetButton.setVisibility(View.VISIBLE);
    }

    /**
     * Resets all scores and makes {@code resetButton} invisible.
     *
     * @param view user interface components.
     */
    public void onClickResetButton(View view) {
        usaScore = 0;
        canadaScore = 0;

        usaView.setText(String.format(Locale.getDefault(), "%d", usaScore));
        canadaView.setText(String.format(Locale.getDefault(), "%d", canadaScore));

        resetButton.setVisibility(View.INVISIBLE);
    }
}